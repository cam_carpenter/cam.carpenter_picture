package picture;
/**
 * PicturePanel.java
 * Author: Chuck Cusack
 * Date: August 22, 2007
 * Version: 2.0
 * 
 * Modified August 22, 2008
 *
 *An almost blank picture.
 *It just draws a few things.
 *
 */
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionListener;

import javax.swing.JPanel;

import java.util.ArrayList;
import java.util.Random;
/**
 * A class draws a picture on a panel
 *
 */
public class PicturePanel extends JPanel 
{
    // A sample field.  I just keep track of number of mouse clicks.
    int numberOfClicks, numberofClicks2;
    private int mouseXLoc, mouseYLoc, clickXLoc, clickYLoc; // keeps track of where the mouse is on the x-axis and y-axis
    private boolean clickL, clickR; // sees if left/right click is true or false
    private javax.swing.Timer myTimer; //Timer for animations
    private ArrayList<People> peoples;
    private ArrayList<Tree> forest;
    
	
    
    /**
     * Get stuff ready so when paintComponent is called, it can draw stuff.
     * Depending on how complicated you want to get, your constructor may be blank.
     */
    public PicturePanel() 
    {
        // If you want to handle mouse events, you will need the following
        // 3 lines of code.  Just leave them as is and modify the methods
        // with "mouse" in the name toward the end of the class.
        // If you don't want to deal with mouse events, delete these lines.
        MouseHandler mh=new MouseHandler();
        addMouseListener(mh);
        addMouseMotionListener(mh);
        peoples = new ArrayList<People>();
        forest = new ArrayList<Tree>();
        
        
        // Initialize number of mouse clicks to 0.
        numberOfClicks=0;
    }

    /**
     * This method is called whenever the applet needs to be drawn.
     * This is the most important method of this class, since without
     * it, we don't see anything.
     * 
     * This is the method where you will most likely do all of your coding.
     */
    public void paintComponent(Graphics g) 
    {
        // Always place this as the first line in paintComponent.
        super.paintComponent(g);
        
        drawBackground(g); //draws the hill and makes it night time
        drawSun(g); // draws the sun
        writeName(g); // prints my name
        
        if (clickL == true)
        {
        	peoples.add(new People(g, clickXLoc, clickYLoc));
        	clickL = false;
        
        }
        if (clickR == true)
        {
        	forest.add(new Tree(g, clickXLoc, clickYLoc));
        	clickR = false;
        
        }
        
        
        
       
        
 
	    // Draw a string which tells how many mouse clicks
	    g.setColor(Color.white);
	    g.setFont(new Font("Times Roman",Font.BOLD,24));
        g.drawString("Number of people made "+numberOfClicks,40,40);
        
        g.setColor(Color.white);
        g.setFont(new Font("Hansief", Font.BOLD, 24));
        g.drawString("Number of trees planted " + numberofClicks2, 40, 80);
    }
    /**
     *  Used to draw the base picture with a hill and night sky
     *
     */
    
    private void drawBackground(Graphics g)
    {
    	
    	g.setColor(Color.blue);
    	g.fillRect(0,0, getWidth(), getHeight()); //used as background
    	g.setColor(Color.green);
    	g.fillOval(-100, 450, 1200, 200); // the hill
    
    	
    }
   
    
    /**
     * Draw the sun in the sky 
     * 
     */
    private void drawSun(Graphics g)
    {
    	g.setColor(Color.yellow);
    	g.fillOval(600, 50, 100, 100); // draws the moon
    }
    
    /**
     * Writes my name in the bottom right corner
     *
     */
    private void writeName(Graphics g)
    {
    	g.setColor(Color.blue);
    	g.setFont(new Font("Arial", Font.ITALIC, 14));
    	g.drawString("Cam Carpenter " , getWidth()-105, getHeight()-20);
    
    }
 
	
    //------------------------------------------------------------------------------------------
    
     //---------------------------------------------------------------
    // A class to handle the mouse events for the applet.
    // This is one of several ways of handling mouse events.
    // If you do not want/need to handle mouse events, delete the following code.
    //
    private class MouseHandler extends MouseAdapter implements MouseMotionListener
    {
      
        public void mouseClicked(MouseEvent e) 
        {
            // Increment the number of mouse clicks
            
            
            clickXLoc = mouseXLoc;
        	clickYLoc = mouseYLoc;
        	if(e.getButton() == 1)
        	{
        		clickL = true;
        		numberOfClicks++;
        	}
        	else if(e.getButton() == 3)
        	{
        		clickR = true;
        		numberofClicks2++;
        	}
            
            
            // After making any changes that will affect the way the screen is drawn,
            // you have to call repaint.
            repaint();
        }

        public void mouseEntered(MouseEvent e)
        {
        }
        public void mouseExited(MouseEvent e) 
        {
        }        
        public void mousePressed(MouseEvent e) 
        {
        	
           
        }        
        public void mouseReleased(MouseEvent e) 
        {
        }
        public void mouseMoved(MouseEvent e) 
        {
        	mouseXLoc = e.getX();
        	mouseYLoc = e.getY();
        }
        public void mouseDragged(MouseEvent e) 
        {
        }
    }
    // End of MouseHandler class
    
    
   
   public class Tree{
	   
	   public Tree(Graphics g, int x, int y) {
		   	drawTree(g, x, y);
		   
	   }
	   
	   public void drawTree(Graphics g, int x, int y) {
	   
		   g.setColor(new Color(139, 69,19));
	       g.fillRect(x,y, 50, 150);		//g.fillRect(300, 300, 50, 150);
	       g.setColor(new Color(0, 100, 0));
	       
	       g.fillOval(x+10, y-45, 75, 75);	//g.fillOval(310, 255, 75, 75); //bottom right
	    	
	       g.fillOval(x-10, y-100, 75, 75);//g.fillOval(290, 200, 75, 75); //top
	       g.fillOval(x-50, y-45, 75, 75);	//g.fillOval(250, 255, 75, 75); //bottom left
	       g.fillOval(x+ 20, y-75, 75, 75);	//g.fillOval(320, 225, 75, 75); // top right
	       g.fillOval(x-60, y-75, 75, 75);	//g.fillOval(240, 225, 75, 75); // top left
	       
	   }
	   
   
   }
    
    	
    
    
    
    public class People{
    
    	private Random randomColor;
    	private ArrayList<Color> colors;
    	
        
        public People(Graphics g, int x, int y){
        	colors = new ArrayList<Color>();
            randomColor= new Random();
            drawPerson(g, x, y);
        
        }
        public void drawPerson(Graphics g, int x, int y) {
    		
    		setColors();
    		g.setColor(pickColor());
    		g.drawLine(x + 15, y + 35, x + 35, y + 50); // arms
    		g.drawLine(x + 15, y + 35, x - 5, y + 50);
    		g.fillRect(x + 13, y + 30, 4, 30); // body
    		g.setColor(Color.yellow);
    		g.drawOval(x, y, 30, 30); // head
    		g.drawOval(x, y, 30, 30);
    		g.drawLine(x + 15, y + 60, x + 31,y + 80); // legs
    		g.drawLine(x + 15 ,y + 60,x + 1,y + 80);
    		g.fillRect(x + 13, y + 30, 4, 6); // neck
    	
    }
        
        
        /**
    	 * Set colors to red white and blue 
    	 */
    	private void  setColors() {
    		colors.add(Color.pink);
    		colors.add(Color.red);
    		colors.add(Color.white);
    		colors.add(Color.gray);
    		colors.add(Color.ORANGE);
    		colors.add(Color.magenta);
    		colors.add(Color.cyan);
    	
    	}
    	/**
    	 * picks a color at random 
    	 */
    	private Color pickColor() {
    		int index = randomColor.nextInt(colors.size());
    		return colors.get(index);
    	
    	}
        
    }
    
   
    
    
} // Keep this line--it is the end of the PicturePanel class







